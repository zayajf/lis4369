from datetime import datetime, time


def main():
    input("Press Enter to start...")
    start = datetime.now()
    month = start.strftime("%B")
    day = start.strftime("%d")
    hours = start.strftime("%I")
    minutes = start.strftime("%M")
    am_pm = start.strftime("%p")
    print("Start time:", month, day, "at", hours, ":", minutes, am_pm)
    input("Press Enter to stop...")
    stop = datetime.now()
    month = stop.strftime("%B")
    day = stop.strftime("%d")
    hours = stop.strftime("%I")
    minutes = stop.strftime("%M")
    am_pm = stop.strftime("%p")
    print("Stop time: ", month, day, "at", hours, ":", minutes, am_pm)
    elapsed_time = stop - start
    new_days = elapsed_time.days
    new_minutes = elapsed_time.seconds // 60
    new_hours = new_minutes // 60
    new_minutes = new_minutes % 60
    print("Time elapsed: ")
    if new_days > 0:
        print("days:", new_days)
    print("hours:", new_hours, ", minutes:", new_minutes)


if __name__ == "__main__":
    main()
