> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>
# Lis4369 - Extensible Enterprise Solutions

## Jonathon Zayas - Information Technology Major

###Project 1 Requirements:

1. Requirments:
    - Use Assignment 5 screenshots and references above for the following requirements:
    - Backware-engineer the lis4369_p2_requirments.txt file
    - Be sure to include at least two plots in your README.md file
2. Be sure to test your program using RStudio

#### README.md file should include the following items:

+ Screenshot of data 
+ data scraping
	
#### Assignment Screenshots:

*Screen Shot of datatable*

![IDLE](img/img1.png)

*Screen shot of data in RStudio*

![VS CODE](img/img2.png)

*Screen Shot of data in RStudio*
