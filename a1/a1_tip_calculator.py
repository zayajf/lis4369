#!/usr/bin/env python3

# Jonathon Zayas
# This program is a tip calculator
# It allows you to enter a cost of meal, tax percentage, Tip percentage and party number.
# The program will then calculate The subtotal, tax, amount due, gratuity, Total and the amount split based on party number.

print("")
print("Tip Calculator")

# Print requirments
print("\nProgram Requirments:\n"
      + "1. Must use float data type for user input (except, \"Party Number\").\n"
      + "2. Must round clculation to two decimal places. \n"
      + "3. Must format currency with dollar sign, and two decimal places.\n")


# taking in user input
print("User Input:")
cost_of_meal = float(input("Cost of meal: "))
tax_percent = float(input("Tax percent: "))
tip_percent = float(input("Tip percent: "))
party = int(input("Party number: "))

# changing user input into a percentage
tax_percent = tax_percent / 100
tip_percent = tip_percent / 100
amount_due = cost_of_meal * tax_percent + \
    cost_of_meal  # calculating amount due
gratuity = amount_due * tip_percent  # calculating gratuity
total = amount_due + gratuity  # calculation total
split = total / party  # calculation split total between party

'''
class example
tax_percent = round(cost_of_meal * (tax_percent/100), 2) #convert to percentage
amount_due = round(meal_cost + tax_amount, 2)
gratuity = round((due_amout) * (tip_percent / 100), 2) #percentage of cost + tax
total = round(meal_cost + tax_amount + tip_amount, 2)
split = round(total/people_num, 2)
'''
# Space between User input and output
print("")

# output
print("Program Output: ")
print("Subtotal: \t${:,.2f}".format(cost_of_meal))
print("Tax: \t\t${:,.2f}".format(tax_percent))
print("Amount due: \t${:,.2f}".format(amount_due))
print("Gratuity: \t${:,.2f}".format(gratuity))
print("Total: \t\t${:,.2f}".format(total))
print("Split (" + str(party) + "): \t${:,.2f}".format(split))
