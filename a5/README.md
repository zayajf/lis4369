> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>
# Lis4369 - Extensible Enterprise Solutions

## Jonathon Zayas - Information Technology Major

### A5 Requirements:

1. Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial
2. Code and run lis4369_a5.R
3. Be sure to include at least two pots in your README.md file
4. R Commands: Save a file of all the R commands included in the tutorial.
5. R Console: save a screenshot of some of the R commands executed above (below requirement) 
6. Graphs: Save at least 5 seperate image files displaying graph plots created from the tutorial.
7. RStudio: save one screenshiot (similar to the one below), displaying the following 4 windows:
    - R source code (top-left corner)
    - Console (bottom-left corner)
    - Enviromeont( or History), (top-right corner)
    - Plots (bottom-right corner)

#### README.md file should include the following items:

+ Screenshot of plots

#### Assignment 5 Screenshots:

*Screen Shot of Rstudio A5*

![VSCode](img/img.png)

*Screen Shot of plot data from a5*

![VSCode](img/img7.png)

*Screen Shot of plot data from a5*

![VSCode](img/img8.png)

*Screen Shot of Rstudio tutorial*

![VSCode](img/img1.png)

*Screen shot of graph*

![VSCode](img/img2.png)

*Screen shot of graph*

![VSCode](img/img3.png)

*Screen shot of graph*

![VSCode](img/img4.png)

*Screen shot of graph*

![VSCode](img/img5.png)

*Screen shot of graph*

![VSCode](img/img6.png)
