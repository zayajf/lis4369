#!/usr/bin/env python
# Jonathon Zayas
# BE SURE the necessary packaes are installed! pip = "Pip Installs Packeges"
# pip freeze

# Starting in 0.19.0, pandas no longer supports pandas.io data or pandas.io.wb.
# Must replce imports from pandas.io with those from pandas...datareader:
# https://pandas-datareader.readthedocs.io/en/latest/remote_data.html
import pandas as pd  # Pandas = "Python Data Analysis Library"
import datetime
import pandas_datareader as pdr  # remote data access for pandas
import matplotlib.pyplot as plt
from matplotlib import style

start = datetime.datetime(2010, 1, 1)
end = datetime.datetime(2018, 10, 15)

# Note: XOM is stock market symbol for Exxon Mobil Corporation
df = pdr.DataReader("XOM", "yahoo", start, end)

print("\nPrint number of records: ")
# statment goes here...

# Why is it important to run the following print statement...
print(df.columns)

print("/nPrint data frame: ")
print(df)  # Note: for efficiency, only prints 60---not *all* records

print("\nPrint first five lines: ")
# Note: "Date" is lower than the other columns as it is treated as an index
# statment goes here...

print("\nPrint last five lines: ")
# statement goes here....

print("\nPrint frist 2 lines: ")
# statment goes here...

print("\nPrint last 2 lines: ")
# statment goes here...

# Research what these styles do!
# style.use("fivethiryeight")
# compare with...
style.use('ggplot')

df['High'].plot()
df['Adj Close'].plot()
plt.legend()
plt.show()
