> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>
# Lis4369 - Extensible Enterprise Solutions

## Jonathon Zayas - Information Technology Major

###Project 1 Requirements:

1. Run Demo.py.
2. If errors, more than likely missing installations.
3. Test Python Package Installer: pip freeze
4. Research how to do the following installations:
    a.  Pandas (only if missing)
    b.  Pandas-datareader (only if missing)
    c. Matplotlib (only if missing)
5. Create at least three functions that are called by the program:
    a. main(): calls at least two other functions.
    b. get_requirements(): displays the program requirements.
    c. data_analysis_1(): displays the following data.

#### README.md file should include the following items:

+ Screenshot of data 
+ data scraping
	
#### Assignment Screenshots:

*Screen Shot of datatable*

![IDLE](img/data.png)

*Screen shot of data in idle*

![VS CODE](img/idle1.png)

*Screen Shot of data in idle*

![VS CODE](img/idle2.png)

*Screen Shot of data in vscode*

![VS CODE](img/vscode.png)

*Screen Shot of data in vscode*

![VS CODE](img/vscode1.png)

*Screen Shot of data in vscode*

![VS CODE](img/vscode2.png)

