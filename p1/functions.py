#!/usr/bin/env python
# Jonathon Zayas


def get_data():

    import pandas as pd  # Pandas = "Python Data Analysis Library"
    import datetime
    import pandas_datareader as pdr  # remote data access for pandas
    import matplotlib.pyplot as plt
    from matplotlib import style
    # Starting in 0.19.0, pandas no longer supports pandas.io data or pandas.io.wb.
    # Must replce imports from pandas.io with those from pandas...datareader:
    # https://pandas-datareader.readthedocs.io/en/latest/remote_data.html

    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime.now()

    # Note: XOM is stock market symbol for Exxon Mobil Corporation
    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")
    print(len(df))

    # Why is it important to run the following print statement...
    print("\n Print columns: ")
    print(df.columns)

    print("/nPrint data frame: ")
    print(df)  # Note: for efficiency, only prints 60---not *all* records

    print("\nPrint first five lines: ")
    # Note: "Date" is lower than the other columns as it is treated as an index
    print(df.head())  # head() Prints top 5 rows. Here, with 7 columns

    print("\nPrint last five lines: ")
    print(df.tail())

    print("\nPrint frist 2 lines: ")
    print(df.head(2))

    print("\nPrint last 2 lines: ")
    print(df.tail(2))

    # Research what these styles do!
    # style.use("fivethiryeight")
    # compare with...
    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()
