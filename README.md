# lis4369

## Joanthon Zayas

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

	- Install Python
	- Install R
	- Install R Studio
	- Install Visual Studio Code
	- Create a1_tip_calculator application
	- Provide screenshots of installations
	- Create Bitbucket tutorial (bitbuckestationlocations)
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	
	- Backward Enginner Python Programs
	- Chapter Questions
	- Push to Bitbucket repo

3. [A3 README.md](a3/README.md "My A3 README.md file")

	- Backward Enginner Python Programs
	- Chapter Questions
	- Push to Bitbucket repo

4. [P1 README.md](p1/README.md "My P1 README.md file")

	- Install required packages
	- Python Program showing formatted data from ExxonMobile ticker
	- Main focus was gathering data using pandas, pandas_datareader, and producing a plot
	- Chapter Questions for chpater 7 and 8

5. [A4 README.md](a4/README.md "My A4 README.md file")

	- Check if required packages are installed
	- Python Program showing formatted data from Titanic manifest
	- Main focus was gathering data using pandas, pandas_datareader, and producing a plot
 	- Chapter Questions for chpater 9 and 10

6. [A5 README.md](a5/README.md "My A5 README.md file")

	- Python
	- R
	- R Studio
	- Big things

7. [P2 README.md](p2/README.md "My P2 README.md file")

	- R
	- R Studio
	- Manipulating data
	- Visualizing data
	- importing data set
