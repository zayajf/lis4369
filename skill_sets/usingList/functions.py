#!/usr/bin/env python
# Jonathon Zayas


def get_requirments():
    print("Program Requirment:\n")
    print("1. Get user beginning and ending interger values, and store in two variables.\n"
          + "2. Display 10 random number between, and including, above values.\n"
          + "3. Must use integer data type."
          + "4. Example 1: Using range() and randint() functions.\n"
          + "5. Example 2: Using a list with range() and shuffle() functions.\n")


def using_list():
    # initialize variables and list
    start = 0
    end = 0
    my_list = []

    # IPO: Input > process > output
    # get user data
    print("Input: \n")
    num = int(input("Enter number of list elements: "))

    #IPO: Input > Process > Output
    # get user data
    # Input and Process
    for i in range(num):
        # prompt user for list element
        my_element = input("Please enter list element" + str(i + 1) + ":")
        my_list.append(my_element)

    # Output
    print("\nOutput: ")
    print("Print my_list: ")
    print(my_list)

    elem = input("\nPlease enter list element: ")
    pos = int(
        input("Please enter list *index* position (note: must convert to int): "))

    print("\n Insert element into specific position in my_list: ")
    my_list.insert(pos, elem)  # Note: pos 1 inserts item into 2nd element
    print(my_list)

    print("\nCount number of elements in list: ")
    # also work for strings, tuples, dict objects
    print(len(my_list))

    print("\nSort elements in list alphabetically: ")
    my_list.sort()  # sort alphabetically
    print(my_list)

    print("\nReverse list: ")
    my_list.reverse()  # Reverse list
    print(my_list)

    print("\nRemove last list element: ")
    my_list.pop()  # delete last element
    print(my_list)

    print("\nDelete second element from list by *index*(note: 1=2nd element): ")
    # pops element at index specified
    # my_list.pop(1)
    # or
    del my_list[1]
    print(my_list)

    print("\nDelete element from list by *value (cherries): ")
    my_list.remove("cherries")
    print(my_list)

    print("\nDelete all elements from list")
    my_list.clear()  # deletes all elements
    print(my_list)
