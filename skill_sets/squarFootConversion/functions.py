#!/bin/python3
# Jonathon Zayas


def conversion():
    print("Program Requirments")
    print("1. Research: number of square feet to acre of land\n"
          + "2. Must use float data type for user input and calculation.\n"
          + "3. Format and round conversion to two decimal places.")

    print("\nInput:")

    square_feet = float(input("Enter square feet: "))
    acre = float(square_feet / 43560)

    print("\nOutput: ")
    print('{:,.2f}'.format(square_feet), "squart feet = ",
          '{:,.2f}'.format(acre), "acres")
