#!/usr/bin/env python
# Jonathon Zayas
import functions as f


def main():
    f.conversion()


if __name__ == "__main__":
    main()
