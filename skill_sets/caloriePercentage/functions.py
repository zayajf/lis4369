#!/usr/bin/env python3
# Jonathon Zayas


def display_menu():
    print("Calorie Percentage")
    print()
    print("Program Requirements:")
    print("1. Find calories per grams of fat, carbs, and protein")
    print("2. Calculate percentages")
    print("3. Must use float data type")
    print("4. Format, right align numbers, and round to two decimal places")
    print()
    print("User Input:")


def convert_percentage():
    fat_grams = float(input("Enter total fat grams: "))
    carb_grams = float(input("Enter total carb grams: "))
    protein_grams = float(input("Enter total protein grams: "))

    fat_calories = fat_grams * 9
    carb_calories = carb_grams * 4
    protein_calories = protein_grams * 4
    total_calories = fat_calories + carb_calories + protein_calories

    percentage_fat = ((fat_grams * fat_calories) / total_calories) / 10
    percentage_carbs = ((carb_grams * carb_calories) / total_calories) / 10
    percentage_protein = (
        (protein_grams * protein_calories) / total_calories) / 10

    print()
    print("Output:\n")
    print("Type\t\t Calories\t\tPercentage")
    print("Fat\t\t", '{:,.2f}'.format(fat_calories),
          '\t\t{:,.2f}%'.format(percentage_fat))
    print("Carbs\t\t", '{:,.2f}'.format(carb_calories),
          '\t\t{:,.2f}%'.format(percentage_carbs))
    print("Protein\t\t", '{:,.2f}'.format(
        protein_calories), '\t\t{:,.2f}%'.format(percentage_protein))
    return
