#!/usr/bin/env python
# Joanthon Zayas

import functions as f


def main():
    f.display_menu()
    f.convert_percentage()


if __name__ == "__main__":
    main()
