#!/usr/bin/env python3
# Joanthon Zayas IT/ICT Percentage


def display_menu():
    print("IT/ICT Student Percentage")
    print()
    print("Program Requirements:")
    print("1. Find number of IT/ICT students in class")
    print("2. Calculate IT/ICT Student Percentage.")
    print("3. Must use float data type (to facilitate right-alignment)")
    print("4. Format, right align numbers, and round to two decimal places")
    print("5. Create at least three functions that are called by the program:")
    print()
    print("User Input:")


def convert_percentage():
    # initialize variables
    it = 0
    ict = 0
    total = 0
    percent_it = 0.0
    percent_ict = 0.0

    it = float(input("Enter number of IT students: "))
    ict = float(input("Enter number of ICT students: "))

    total = it + ict
    percent_it = (it * 100) / total
    percent_ict = (ict * 100) / total

    print()
    print("Total Students:      ", '{:,.2f}'.format(total))
    print("IT Students:         ", '{:,.2f}%'.format(percent_it))
    print("ICT Students:        ", '{:,.2f}%'.format(percent_ict))
    return
