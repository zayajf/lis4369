#!/usr/bin/env python
# Jonathon Zayas
import functions as f


def main():
    f.get_requirments()
    f.random_numbers()


if __name__ == "__main__":
    main()
