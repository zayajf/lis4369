#!/usr/bin/env python
# Jonathon Zayas
import random


def get_requirments():
    print("\nProgram Requirment:\n")
    print("1. Get user beginning and ending interger values, and store in two variables.\n"
          + "2. Display 10 random number between, and including, above values.\n"
          + "3. Must use integer data type."
          + "4. Example 1: Using range() and randint() functions.\n"
          + "5. Example 2: Using a list with range() and shuffle() functions.\n")


def random_numbers():
    # initialize
    start = 0
    end = 0

    # IPO: Input > process > Ouput
    # get user data
    print("Input:\n")

    start = int(input("Enter beginning value: "))
    end = int(input("Enter ending value: "))

    # Process and Output
    # dsiplay 10 random numbers between start and end, eclusive
    print("\nOutput: ")
    print("\nExample 1: Using range() and randint() functions: \n")
    for count in range(10):
        print(random.randint(start, end), sep=",", end=" ")

    print()

    print("\nExample 2: Using a list, with range() and shuffle() functions:\n")
    # shuffle() randomizes items of a list in place (must include list)
    # range generates list of integers from 1, but not including, 11
    r = list(range(start, end + 1))  # because list (zero-based) must add 1
    random.shuffle(r)
    for i in r:
        print(i, sep=",", end="")

    print()
