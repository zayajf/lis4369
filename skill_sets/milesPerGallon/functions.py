#!/usr/bin/env python3
# Jonathon Zayas


def display_menu():
    print("Miles Per Gallon")
    print("")
    print("Program Requirements:")
    print("1. Convert MPG")
    print("2. Must use float data type for user input and calculation")
    print("4. Format, right align numbers, and round to two decimal places")
    print("")
    print("User Input:")


def convert_mpg():
    miles_driven = float(input("Enter miles driven: "))
    gallons_used = float(input("Enter gallons of fuel used: "))

    mpg = miles_driven / gallons_used

    print()
    print('{:,.2f}'.format(miles_driven), " miles driven and ",
          '{:,.2f}'.format(gallons_used), " gallons used = ",
          '{:,.2f}'.format(mpg), " mpg")
    return 
