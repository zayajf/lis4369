> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>
# Lis4369 - Extensible Enterprise Solutions

## Jonathon Zayas - Information Technology Major

###Assignment 2 Requirements:

1. Must use float data type for user input.
2. Overtime rate: 1.5 times hourly rate (hours over 40).
3. Holiday rate: 2.0 times hourly rate (all holiday hours).
4. Must format currency with dollar sign, and round to two decimal places.
5. Create at least three functions that are called by the program.
    a. main(): calls at least two other funcitons.
    b. get_requirments(): displays the program requirments.
    c. calculate_payroll(): calculates an individual one-week paycheck.

#### README.md file should include the following items:

+ Screenshots of payroll.py application running
	
#### Assignment Screenshots:

*Screen Shot of payroll running in idle*

![IDLE](img/idle.png)

*Screen shot of payroll running in Visual Studio Code*

![VS CODE](img/vs.png)
