#!/usr/bin/env python3
# Jonathon Zayas
import functions as f


def main():
    f.get_requirments()
    f.calculate_payroll()


if __name__ == "__main__":
    main()
