#!/usr/bin/env python3
# Jonathon Zayas


def get_requirments():
    # Space between title and program requirments
    print("")
    print("Payroll")
    print("\nProgram Requirments:\n"
          + "1. Must use float data type for user input.\n"
          + "2. Overtime rate: 1.5 times hourly rate (hours over 40). \n"
          + "3. Holiday rate: 2.0 times hourly rate (all holiday hours). \n"
          + "4. Must format currency with dollar sign, and round to two decimal places. \n"
          + "5. Create at least three functions that are called by the program: \n"
          + "\t a. main(): calls at least two other funcitons.\n "
          + "\t b. get_requirments(): displays the program requirments. \n"
          + "\t c. calculate_payroll(): calculates an individual one-week paycheck. \n")


def calculate_payroll():
    # taking in user input
    # constants represent base hours, overtime and holiday rates (note: python doesn't provide true constants)
    BASE_PAY = 40
    OT_TIME = 1.5
    HOLIDAY_RATE = 2.0

    #IPO: Input > Process > Output
    # get user data
    print("User Input: ")
    hours_worked = float(input("Enter hours worked: "))
    holiday_hours = float(input("Enter holiday hours: "))
    pay_rate = float(input("Enter hourly pay rate: "))

    if hours_worked > BASE_PAY:
        hours_over = hours_worked - BASE_PAY
        pay = BASE_PAY * pay_rate
        overtime = hours_over * pay_rate * OT_TIME
        holiday_pay = holiday_hours * HOLIDAY_RATE * pay_rate
        gross = pay + holiday_pay + overtime

    elif hours_worked <= BASE_PAY:
        pay = BASE_PAY * pay_rate
        holiday_pay = holiday_hours * HOLIDAY_RATE * pay_rate
        gross = pay + holiday_pay
        overtime = 0

    # Space between Requirments and user input

    # output
    print("")
    print("Output")
    print("Base: \t\t${:,.2f}".format(pay))
    print("Overtime: \t${:,.2f}".format(overtime))
    print("Holiday Pay: \t${:,.2f}".format(holiday_pay))
    print("Gross: \t\t${:,.2f}".format(gross))
