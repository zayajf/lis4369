> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>
# Lis4369 - Extensible Enterprise Solutions

## Jonathon Zayas - Information Technology Major

###A4 Requirements:

1. Python Program demo.py that uses the data from the Titanic - Main focus was python data manipulators and csv file reading
2. Chapter Questions 9, 10
3. Bitbucket Repo Links: - this assignment

#### README.md file should include the following items:

+ Screenshot of data 
+ data scraping

#### Assignment Screenshots:

*Screen Shot of demo.py requirments*

![VSCode](img/vscode.png)

*Screen shot of graph*

![VSCode](img/vscode1.png)