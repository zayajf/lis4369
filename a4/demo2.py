#!/bin/bash/env python3
# Jonathon Zayas
# A4

print("\n19.*** Convert pandas DataFrame df to Numpy ndarray, use values command:***")
# Select all rows, and all coumns, starting at column 2:
b = df.iloc[:, 1:].values  # ndarray = N-dimensional array (rows and columns)

print("\n20. Print data frame types:")
print(type(df))

print("\n21. Print a type:")
print(type(a))

print("\n22. Print b type:")
print(type(b))

print("\n23. Print number of dimensions and items in array (rows, columns). Remeber: starting at column 2:")
print(b.shape)

print("\n24. Print type of items in array. Rember: ndarray is an array of arrays. Each record/ item is an array.")
print(b.dtype)

print("\n25. Printing a:")
print(a)

print("\n26. Length a:")
print(len(a))

print("\n27. Printing b:")
print(b)

print("\n28. Lenght b:")
print(len(b))

# Print element of ndarray b in *second* row, *third* column
print("\n29. Print element of (NumPy array) ndarray b in *second* row, *third* column:")
print(b[1, 2])

# Print full NumPY array, no ellipsis: here is why np.set_printoptions(threshold=np.inf) is set at top of file
print("\n30. Print all records for Numpy array column 2:")
print(b[1, 2])

print("\n31. Get passenger names:")
names = df["Name"]
print(names)

print("\n32. Find all passengers with name 'Allison' (using regular exprssions):")
# Note: 'r' obviates the need for an escape sequence. For example: \'(Allison)\'
# See: https://docs.python.org/2/library/re.html
for name in names:
    print(re.search(r'(Allison)', name))
# Note: there are various ways of retrieving data

# Note: print full DataFrame, w/no ellipsis
# will automatically return options to their default values
# with pd.option_context('display.max_rows', None):
# print(df) # print entire dataframe

print("\n***33. Statistical Analysis (DataFrame notation):***")
# difference between np.mean and np.average: average takes optional weight parameter. If not supplied they are equivalent.
print("\na) Print mean age:")
avg = df["Age"].mean()  # second column
print(avg)

print("\nb) Print mean age, rounded to two decimal places:")
avg = df["Age"].mean().round(2)  # will *not* display last 0
print(avg)

print("\nC) Print mean of every column in DataFrame (may not be suitable with certain columns):")
avg_all = df.mean(axis=0)  # mean every column
# avg_all = df.mean(axis=1) # mean every row
print(avg_all)

print("\nd) Print summary statistics (DataFrame notation):")
# returns three quartiles, mean, count, min/max values, and standard deviation
describe = df["Age"].describe()  # seond column
# describe = df["Age"].describe(percentiles=[.10, .20, .50, .80]) # choose different percentiles
print(describe)

print("\ne) Print minimum age (DataFrame notation):")
# can also do functions seperately
min = df["Age"].min()  # second column
print(min)

print("\nf) Print maximum age (DataFrame notation):")
max = df["Age"].max()  # second column
print(max)

print("\ng) Print median age (DataFrame notation):")
median = df["Age"].median()  # second column

print("\nh) Print mode age (DataFrame notation):")
mode = df["Age"].mode()  # second column

print("\ni) Print number of values (DataFrame notaiton):")
count = df["Age"].ount()  # second column
print(count)

print("\n***Graph: Display ages of the first 20 passengers (use code from previous assignment):***")
