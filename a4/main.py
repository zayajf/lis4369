#!/usr/bin/env python3
# Jonathon Zayas
# A4
import functions as f


def main():
    f.get_requirements()
    f.data_analysis_2()


if __name__ == "__main__":
    main()
