> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>
# Lis4369 - Extensible Enterprise Solutions

## Jonathon Zayas - Information Technology Major

###Assignment 3 Requirements:

1. Calculate home interrior aint cost (w/o primer).
2. Must use float data types.
3. Must use SQFT_PER_GALLON constant (350).
4. Must use iteration structure (aka "loop").
5. Format, right-align number,  and round to two decimal places.
6. Create at least three functions that are called by the program:
        a. main(): calls at least two other functions.
        b. get_requirements(): displays the  program  requirements.
        c. estimate_painting_cost(): calculates interior home painting.

#### README.md file should include the following items:

+ Screenshots of Painting estimator application running
	
#### Assignment Screenshots:

*Screen Shot of Paint Estimator running in idle*

![IDLE](img/idle.png)

*Screen shot of Paint Estimator running in Visual Studio Code*

![VS CODE](img/vs.png)
