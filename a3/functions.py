#!/usr/bin/env python
# Jonathon Zayas


def get_requirements():
    print("Program Requirments: \n"
          + "1. Calculate home  interrior paint cost (w/o primer).\n"
          + "2. Must use float data types.\n"
          + "3. Must use SQFT_PER_GALLON constant (350).\n"
          + '4. Must use iteration structure (aka "loop").\n'
          + "5. Format, right-align number,  and round to two decimal places.\n"
          + "6. Create at least three functions that are called by the program:\n"
          + "\ta. main(): calls at least two other functions.\n"
          + "\tb. get_requirements(): displays the  program  requirements.\n"
          + "\tc. estimate_painting_cost(): calculates interior home painting.\n")


def estimate_painting_cost():

    SQFT_PER_GALLON = 350.00
    total_sqft = 0.0
    price_per_gallon = 0.0
    hourly_rate = 0.0

    # input
    print("Input: ")
    total_sqft = float(input("Enter total interior sq ft: "))
    price_per_gallon = float(input("Enter price per gallon paint: "))
    hourly_rate = float(input("Enter hourly painting rate per sq ft: "))

    # processing
    gallons_of_paint = total_sqft / SQFT_PER_GALLON
    paint_cost = gallons_of_paint * price_per_gallon
    labor_cost = total_sqft * hourly_rate
    total_cost = labor_cost + paint_cost
    paint_percent = paint_cost / total_cost
    labor_percent = labor_cost / total_cost

    print("{:20}{:>9}".format("Item", "Amount"))
    print("{:20}{:9,.2f}".format("Total Sq FT: ", total_sqft))
    print("{:20}{:9,.2f}".format("Sq Ft per Gallon: ", SQFT_PER_GALLON))
    print("{:20}{:9,.2f}".format("Number of Gallons: ", price_per_gallon))
    print("{:20}${:8,.2f}".format("Paint per Gallon: ", paint_cost))
    print("{:20}${:8,.2f}".format("Labor per Sq Ft: ", labor_cost))

    print("")

    print("{:8}{:>9}{:>13}".format("Cost", "Amout", "Percentage"))
    print("{:8}${:8,.2f}{:13.2%}".format(
        "Paint: ", paint_cost, paint_percent))
    print("{:8}${:8,.2f}{:13.2%}".format(
        "Labor: ", labor_cost, labor_percent))
    print("{:8}${:8,.2f}{:13.2%}".format(
        "Total: ", total_cost, labor_percent + paint_percent))
