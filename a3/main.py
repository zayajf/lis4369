#!/usr/bin/env python
# Jonathon Zayas
import functions as f


def main():
    f.get_requirements()
    again = "y"
    while again.lower() == "y":
        f.estimate_painting_cost()
        print("")
        again = input("Estimate another paint job? (y/n): ")
        print("")

    print("Thank you for using our Painting Estimator!")
    print("Please see our web site: http://www.mysite.com")


if __name__ == "__main__":
    main()
